# About project
This project is representing my own resume as application for Android's devices. As a future interviewer You can look into my code, see style of coding, used patterns or principles, roast me etc.

Data displayed in each fragments comes from Firebase Firestore to be easy updated any time by me.

## Architecture
Architecture pattern used here is MVVM. Project contains 4 major modules:

* **app** - connect all modules, initialize application, manifest and provide DI modules
* **domain** - business logic of application, containing repository interface, core entities
* **data** - implementation for domain module. Depends only on domain module
* **ui** - whole user interface with single activity and fragment per each functionality. Depends only on domain module. Contains few base abstraction class like for core Fragment, ViewModel or RecyclerView with delegates pattern

There is also minor module **buildSrc** to provide gradle.kts syntax in build files.

Data displayed in each fragments comes from Firebase Firestore to be easy updated any time by me.

## Utils
To provide clean code pattern there is used ktlint and detekt plugin.