package com.kuba.cv

import android.app.Application
import com.kuba.cv.di.AppModule
import com.kuba.cv.di.ViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(AppModule, ViewModelModule)
        }
    }
}
