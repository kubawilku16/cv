package com.kuba.cv.di

import com.google.firebase.firestore.FirebaseFirestore
import com.kuba.cv.data.RepositoryImpl
import com.kuba.cv.domain.Repository
import org.koin.dsl.module

val AppModule = module {
    single<Repository> { RepositoryImpl(FirebaseFirestore.getInstance()) }
}
