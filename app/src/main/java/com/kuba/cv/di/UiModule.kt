package com.kuba.cv.di

import com.kuba.cv.ui.core.BaseViewModel
import com.kuba.cv.ui.education.EducationViewModel
import com.kuba.cv.ui.experience.ExperienceViewModel
import com.kuba.cv.ui.profile.ProfileViewModel
import com.kuba.cv.ui.skills.SkillsViewModel
import com.kuba.cv.ui.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val ViewModelModule = module {
    viewModel { BaseViewModel() }
    viewModel { SplashViewModel() }
    viewModel { ProfileViewModel(get()) }
    viewModel { ExperienceViewModel(get()) }
    viewModel { SkillsViewModel(get()) }
    viewModel { EducationViewModel(get()) }
}
