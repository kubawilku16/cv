object App {
    const val applicationId = "com.kuba.cv"
    const val versionCode = 1
    const val versionName = "1.0.0"

    const val compileSdk = 29
    const val buildToolSdk = "29.0.2"
    const val minSdk = 21
    const val targetSdk = 29
}

private object Versions {
    const val kotlinPlugin = "1.3.72"
    const val gradlePlugin = "4.0.1"
    const val googleServices = "4.3.3"

    const val kotlin = "1.3.72"
    const val supportLibrary = "1.0.0"
    const val koin = "2.0.0"
    const val jetpack = "2.0.0"
    const val rxAndroid = "2.1.1"
    const val dataBinding = "3.0.1"
    const val glide = "4.11.0"
    const val firestore = "21.3.1"
    const val circleImage = "3.1.0"
}

object RootDependencies {
    const val gradle = "com.android.tools.build:gradle:${Versions.gradlePlugin}"
    const val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinPlugin}"
    const val googleServices = "com.google.gms:google-services:${Versions.googleServices}"
}

object UiDependencies {
    const val supportV4 = "androidx.legacy:legacy-support-v4:${Versions.supportLibrary}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.supportLibrary}"
    const val dataBinding = "com.android.databinding:compiler:${Versions.dataBinding}"
    const val jetpackFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.jetpack}"
    const val jetpackUI = "androidx.navigation:navigation-ui-ktx:${Versions.jetpack}"
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideProcessor = "com.github.bumptech.glide:compiler:${Versions.glide}"
    const val circleImage = "de.hdodenhof:circleimageview:${Versions.circleImage}"
}

object CommonDependencies {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    const val kotlinReflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"
    const val koin = "org.koin:koin-android:${Versions.koin}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    const val firestore = "com.google.firebase:firebase-firestore:${Versions.firestore}"
}
