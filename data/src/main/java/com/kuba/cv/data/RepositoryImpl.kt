package com.kuba.cv.data

import com.google.firebase.firestore.FirebaseFirestore
import com.kuba.cv.domain.Repository
import com.kuba.cv.domain.entity.EducationStep
import com.kuba.cv.domain.entity.JobExperience
import com.kuba.cv.domain.entity.Profile
import com.kuba.cv.domain.entity.SkillSet
import io.reactivex.Single
import java.io.InvalidClassException

class RepositoryImpl(private val db: FirebaseFirestore) : Repository {

    override fun <DataType : Any> extractCollection(clazz: Class<DataType>): Single<Collection<DataType>> {
        return Single.create { emitter ->
            val path: String? = when (clazz) {
                JobExperience::class.java -> "experience"
                SkillSet::class.java -> "skills"
                EducationStep::class.java -> "education"
                else -> null
            }
            path?.let {
                db.collection(path).get()
                    .addOnSuccessListener {
                        emitter.onSuccess(it.documents.mapNotNull { doc -> doc.toObject(clazz) })
                    }
                    .addOnFailureListener { emitter.onError(it) }
            }
                ?: emitter.onError(InvalidClassException("Unexpected data type to extract from db"))
        }
    }

    override fun <DataType : Any> extractData(clazz: Class<DataType>): Single<DataType> {
        return Single.create { emitter ->
            val path: String? = when (clazz) {
                Profile::class.java -> "profile"
                else -> null
            }
            path?.let {
                db.collection(path).get()
                    .addOnSuccessListener {
                        emitter.onSuccess(it.documents
                            .mapNotNull { doc -> doc.toObject(clazz) }
                            .first()
                        )
                    }
                    .addOnFailureListener { emitter.onError(it) }
            }
                ?: emitter.onError(InvalidClassException("Unexpected data type to extract from db"))
        }
    }
}
