plugins {
    id("kotlin")
}

apply(from = "../ktlint.gradle")

dependencies {
    implementation(CommonDependencies.kotlin)
    implementation(CommonDependencies.rxAndroid)
}
