package com.kuba.cv.domain

import io.reactivex.Single

/**
 * Connector between data source and ViewModel
 */
interface Repository {

    /**
     * Extracts single item from database for given [DataType]
     *
     * @param clazz - class of extracted item
     */
    fun <DataType : Any> extractData(clazz: Class<DataType>): Single<DataType>

    /**
     * Extracts collection of items from database for given [DataType]
     *
     * @param clazz - class of extracted items
     */
    fun <DataType : Any> extractCollection(clazz: Class<DataType>): Single<Collection<DataType>>
}
