package com.kuba.cv.domain.entity

data class EducationStep(
    val title: String = "",
    val place: String = "",
    val subTitle: String = "",
    val content: String = "",
    val startDate: Long = 0L,
    val endDate: Long? = null
)
