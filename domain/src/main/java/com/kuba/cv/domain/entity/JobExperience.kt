package com.kuba.cv.domain.entity

data class JobExperience(
    val company: String = "",
    val place: String = "",
    val role: String = "",
    val description: String = "",
    val image: String = "",
    val startDate: Long = 0L,
    val endDate: Long? = null
)
