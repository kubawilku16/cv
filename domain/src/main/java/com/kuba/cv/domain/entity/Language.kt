package com.kuba.cv.domain.entity

data class Language(val name: String, val level: String)
