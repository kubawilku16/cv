package com.kuba.cv.domain.entity

data class Profile(
    val firstName: String = "",
    val lastName: String = "",
    val phone: String = "",
    val email: String = "",
    val git: String = "",
    val linkedIn: String = "",
    val photo: String = "",
    val about: String = "",
    val languages: Map<String, String> = mutableMapOf()
)
