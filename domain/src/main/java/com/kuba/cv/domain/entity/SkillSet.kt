package com.kuba.cv.domain.entity

data class SkillSet(
    val title: String = "",
    var values: List<String> = listOf(),
    val position: Int = 0
)
