package com.kuba.cv.ui.core

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Base class for [ViewModel] to interact with app's domain
 */
open class BaseViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val keyboardHidingNotifier = MutableLiveData<Unit>()

    protected fun start(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }
}
