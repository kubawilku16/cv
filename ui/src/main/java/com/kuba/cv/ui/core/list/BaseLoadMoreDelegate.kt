package com.kuba.cv.ui.core.list

import android.view.View

abstract class BaseLoadMoreDelegate :
    BaseRecyclerViewDelegate<ListProgress, BaseRecyclerViewHolder<ListProgress>>(ListProgress::class.java) {
    override fun createViewHolder(view: View) = BaseRecyclerViewHolder<ListProgress>(view)
}
