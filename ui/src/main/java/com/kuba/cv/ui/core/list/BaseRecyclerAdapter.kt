package com.kuba.cv.ui.core.list

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Base adapter for creating lists with [BaseRecyclerViewDelegate] delegates for each elements
 */
class BaseRecyclerAdapter : RecyclerView.Adapter<BaseRecyclerViewHolder<Any>>() {

    val delegates = mutableListOf<BaseRecyclerViewDelegate<Any, in BaseRecyclerViewHolder<Any>>>()
    private var items: MutableList<Any> = mutableListOf()
    private var nextViewType = 0

    /**
     * Adds items to list
     *
     * @param list of items to be added
     */
    fun addItems(list: List<Any>) {
        this.items.let {
            val startPosition = it.size + 1
            it.addAll(list)
            val endPosition = it.size
            if (startPosition == 1) {
                notifyDataSetChanged()
            } else {
                notifyItemRangeChanged(startPosition, endPosition)
            }
        }
    }

    /**
     * Updates list [item] at [position]
     *
     * @param position to update
     * @param item new value
     */
    fun updateItemAt(position: Int, item: Any) {
        items[position] = item
        notifyItemChanged(position)
    }

    /**
     * Adds [item] to list at [position]
     *
     * @param position of new item
     * @param item new value
     */
    fun addItemAt(position: Int, item: Any) {
        items.add(position, item)
        notifyItemInserted(position)
        if (position <= items.size - 1) {
            notifyItemRangeChanged(position + 1, items.size)
        }
    }

    /**
     * Remove item from list at specified position
     *
     * @param position of item to remove
     */
    fun removeItem(position: Int) {
        items.let {
            it.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, it.size)
        }
    }

    /**
     * Refreshes items in adapter
     *
     * @param list of new items
     */
    fun reloadItems(list: List<Any>) {
        items.clear()
        addItems(list)
    }

    /**
     * Returns current adapter items
     */
    fun getItems(): List<Any> {
        return items.toList()
    }

    /**
     * Adds another delegate to list of available delegates
     *
     * @param delegate
     */
    fun <T : BaseRecyclerViewDelegate<*, *>> addDelegate(delegate: T) {
        if (nextViewType == Integer.MAX_VALUE) {
            nextViewType = 0
        }
        delegate.viewType = nextViewType++
        @Suppress("UNCHECKED_CAST")
        delegates.add(delegate as BaseRecyclerViewDelegate<Any, in BaseRecyclerViewHolder<Any>>)
    }

    override fun onBindViewHolder(holder: BaseRecyclerViewHolder<Any>, position: Int) {
        items.let { items ->
            delegates.filter { it.viewType == holder.itemViewType }
                .forEach { it.onBindViewHolder(items[position], holder, position, items.size) }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return delegates.firstOrNull { it.isForViewType(items, position) }?.viewType ?: -1
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRecyclerViewHolder<Any> {
        delegates.firstOrNull { it.viewType == viewType }
            ?.let { return it.onCreateViewHolder(parent) as BaseRecyclerViewHolder<Any> }

        throw IllegalArgumentException("No delegate found. Required view type: $viewType. " +
                "Available delegates view types: " +
                delegates.joinToString(", ") { "${it.clazz.simpleName}(${it.viewType})" })
    }

    override fun onViewRecycled(holder: BaseRecyclerViewHolder<Any>) {
        super.onViewRecycled(holder)
        delegates.filter { it.viewType == holder.itemViewType }
            .forEach { it.onViewRecycled(holder) }
    }

    override fun getItemCount() = items.size
}
