package com.kuba.cv.ui.core.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Base delegate implementation providing specific list's item view
 *
 * @param DataType is data with which [BaseRecyclerAdapter] should recognize specific delegate
 * @param Holder is [RecyclerView.ViewHolder] which has implemented [LayoutContainer]
 */
abstract class BaseRecyclerViewDelegate<DataType, Holder : BaseRecyclerViewHolder<in DataType>>
    (val clazz: Class<DataType>) {

    var viewType: Int = 0

    protected abstract val layoutId: Int

    open fun onViewRecycled(holder: Holder) {}

    protected abstract fun createViewHolder(view: View): Holder

    protected open fun bindViewHolder(
        item: DataType,
        holder: Holder,
        position: Int,
        itemsSize: Int
    ) {
        holder.bindView(item, position, itemsSize)
    }

    fun onCreateViewHolder(parent: ViewGroup): Holder {
        return createViewHolder(
            LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        )
    }

    fun isForViewType(items: List<*>, position: Int): Boolean {
        return clazz.isInstance(items[position])
    }

    @Suppress("UNCHECKED_CAST")
    fun onBindViewHolder(
        item: DataType,
        holder: RecyclerView.ViewHolder,
        position: Int,
        itemsSize: Int
    ) {
        bindViewHolder(item, holder as Holder, position, itemsSize)
    }

    open fun getItemId(item: DataType): Long {
        return -1
    }
}
