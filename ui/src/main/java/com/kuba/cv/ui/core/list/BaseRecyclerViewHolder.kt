package com.kuba.cv.ui.core.list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

/**
 * Base [RecyclerView.ViewHolder] implementation including [LayoutContainer] to provide easier access to xml
 *
 * @param DataType is type of data provided data to bind view
 */
open class BaseRecyclerViewHolder<DataType>(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {

    open fun bindView(item: DataType, position: Int, itemsSize: Int) {}
}
