package com.kuba.cv.ui.core.list

import android.graphics.Canvas
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.kuba.cv.ui.R

class CustomDivider(
    @DrawableRes private val drawableRes: Int = R.drawable.bg_divider_green,
    @DimenRes private val marginRes: Int = R.dimen.double_spacer
) : RecyclerView.ItemDecoration() {

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        var dividerLeft = parent.paddingLeft
        var dividerRight = parent.width - parent.paddingRight
        if (marginRes > 0) {
            with(parent.context.resources.getDimension(marginRes)) {
                dividerLeft += this.toInt()
                dividerRight -= this.toInt()
            }
        }

        val childCount = parent.childCount
        val divider = ContextCompat.getDrawable(parent.context, drawableRes)!!
        for (i in 0..childCount - 2) {
            val child = parent.getChildAt(i)
            val dividerTop =
                child.bottom + (child.layoutParams as RecyclerView.LayoutParams).bottomMargin
            val dividerBottom = dividerTop + divider.intrinsicHeight

            divider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
            divider.draw(canvas)
        }
    }
}
