package com.kuba.cv.ui.core.list

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Custom implementation of [RecyclerView]
 */
class CustomRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    private var containerView: View? = null
    private var emptyView: View? = null

    var scrolledDownListener: ScrolledDownListener? = null

    @JvmField
    var isLoadingAllFinished: Boolean = false

    @JvmField
    var isLoadingMore: Boolean = false

    private var itemProgress: ListProgress = ListProgress()

    init {
        if (!isInEditMode) {
            addOnScrollListener(object : OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    post {
                        if (isLastItemDisplaying() && !isLoadingAllFinished && !isLoadingMore) {
                            scrolledDownListener?.let {
                                loadingMore()
                                it.viewIsOnTheBottom()
                            }
                        }
                    }
                }
            })
        }
    }

    /**
     * Observer on data changes
     */
    private val observer = object : RecyclerView.AdapterDataObserver() {
        override fun onChanged() {
            checkIfEmpty()
        }

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            checkIfEmpty()
        }

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
            checkIfEmpty()
        }
    }

    override fun setAdapter(adapter: Adapter<*>?) {
        val oldAdapter = getAdapter()
        oldAdapter?.unregisterAdapterDataObserver(observer)
        super.setAdapter(adapter)
        adapter?.registerAdapterDataObserver(observer)
        adapter?.let { checkIfEmpty() }
    }

    /**
     * Checks if list of elements is empty, if is empty then hides recycler view
     */
    fun checkIfEmpty() {
        if (emptyView != null && adapter != null) {
            val emptyViewVisible = adapter!!.itemCount == 0
            emptyView?.visibility = if (emptyViewVisible) View.VISIBLE else View.GONE
            if (containerView != null) {
                containerView?.visibility = if (emptyViewVisible) View.GONE else View.VISIBLE
            } else {
                visibility = if (emptyViewVisible) View.GONE else View.VISIBLE
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : BaseLoadMoreDelegate> addLoadMoreDelegate(delegate: T) {
        (adapter as? BaseRecyclerAdapter)?.let {
            if (it.delegates.singleOrNull { delegate -> delegate is BaseLoadMoreDelegate } == null) {
                it.addDelegate(delegate)
            }
        }
    }

    /**
     * Sets empty view for showing if list of elements will be empty
     */
    fun setEmptyView(emptyView: View?) {
        this.emptyView = emptyView
        checkIfEmpty()
    }

    fun setRecyclerViewContainer(containerView: View) {
        this.containerView = containerView
    }

    /**
     * Checks if last item was displayed
     *
     * @return true if there is no more elements to show, false otherwise
     */
    fun isLastItemDisplaying(): Boolean {
        if (this.adapter!!.itemCount != 0) {
            var lastVisibleItemPosition = 0
            if (this.layoutManager is LinearLayoutManager) {
                lastVisibleItemPosition =
                    (this.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
            } else if (this.layoutManager is GridLayoutManager) {
                lastVisibleItemPosition =
                    (this.layoutManager as GridLayoutManager).findLastCompletelyVisibleItemPosition()
            }
            if (lastVisibleItemPosition != NO_POSITION && lastVisibleItemPosition == this.adapter!!.itemCount - 1)
                return true
        }
        return false
    }

    fun setScrollDownListener(scrolledDownListener: ScrolledDownListener?) {
        this.scrolledDownListener = scrolledDownListener
    }

    /**
     * Determines to show new loaded items
     */
    fun loadingMore() {
        isLoadingMore = true
        adapter?.let {
            if (it.itemCount > 0 &&
                it is BaseRecyclerAdapter &&
                it.getItems().last() !is ListProgress
            ) {
                it.addItems(listOf(itemProgress))
                layoutManager?.run { scrollToPosition(it.itemCount - 1) }
            }
        }
    }

    /**
     * Hides loading progress if is still visible after end of loading new elements
     */
    fun loadingFinished(isLoadingAllFinished: Boolean) {
        adapter?.let { adapter ->
            if (adapter is BaseRecyclerAdapter) {
                val pos = adapter.getItems().indexOfFirst { it is ListProgress }
                if (pos > -1) {
                    adapter.removeItem(pos)
                }
            }
        }
        isLoadingMore = false
        this.isLoadingAllFinished = isLoadingAllFinished
    }

    /**
     * @interface for scrolling listening
     */
    interface ScrolledDownListener {
        fun viewIsOnTheBottom()
    }
}
