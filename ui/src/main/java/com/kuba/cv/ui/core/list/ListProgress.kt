package com.kuba.cv.ui.core.list

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Delegate to represnt loading item at [CustomRecyclerView]
 */
@Parcelize
class ListProgress : Parcelable
