package com.kuba.cv.ui.education

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.kuba.cv.ui.R
import com.kuba.cv.ui.core.BaseFragment
import com.kuba.cv.ui.core.list.BaseRecyclerAdapter
import com.kuba.cv.ui.core.list.CustomDivider
import com.kuba.cv.ui.databinding.FragmentEducationBinding
import kotlinx.android.synthetic.main.fragment_education.*
import org.koin.androidx.viewmodel.ext.android.getViewModel

class EducationFragment : BaseFragment<FragmentEducationBinding>() {
    override val layoutId: Int = R.layout.fragment_education

    private val adapter = BaseRecyclerAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel<EducationViewModel>().run {
            error.observe(this@EducationFragment, Observer {
                it?.let { educationError.text = getString(R.string.error_education) }
            })
            educationSteps.observe(this@EducationFragment, Observer { adapter.reloadItems(it) })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.model = getViewModel()
        adapter.addDelegate(EducationStepDelegate())
        educationRecycler.addItemDecoration(CustomDivider())
        educationRecycler.adapter = adapter
    }
}
