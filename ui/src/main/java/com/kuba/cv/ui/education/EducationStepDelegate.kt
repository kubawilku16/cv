package com.kuba.cv.ui.education

import android.view.View
import com.kuba.cv.domain.entity.EducationStep
import com.kuba.cv.ui.R
import com.kuba.cv.ui.core.list.BaseRecyclerViewDelegate
import com.kuba.cv.ui.core.list.BaseRecyclerViewHolder
import kotlinx.android.synthetic.main.list_education_step.*
import java.text.SimpleDateFormat
import java.util.Locale

class EducationStepDelegate :
    BaseRecyclerViewDelegate<EducationStep, EducationStepDelegate.ViewHolder>(EducationStep::class.java) {

    override val layoutId: Int = R.layout.list_education_step

    override fun createViewHolder(view: View): ViewHolder = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseRecyclerViewHolder<EducationStep>(view) {
        override fun bindView(item: EducationStep, position: Int, itemsSize: Int) {
            educationStepTitle.text = item.title
            educationStepSubtitle.text = item.subTitle
            educationStepContent.text = item.content
            educationStepPlace.text = item.place
            val formatter = SimpleDateFormat("MMM-YY", Locale.getDefault())
            educationStepDate.text = if (item.endDate == null) {
                formatter.format(item.startDate)
            } else {
                "${formatter.format(item.startDate)} -\n${formatter.format(item.endDate)}"
            }
        }
    }
}
