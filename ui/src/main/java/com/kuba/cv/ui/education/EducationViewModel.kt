package com.kuba.cv.ui.education

import androidx.lifecycle.MutableLiveData
import com.kuba.cv.domain.Repository
import com.kuba.cv.domain.entity.EducationStep
import com.kuba.cv.ui.core.BaseViewModel
import com.kuba.cv.ui.utils.resolve

class EducationViewModel(private val repository: Repository) : BaseViewModel() {
    val isLoading = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable?>()
    val educationSteps = MutableLiveData<List<EducationStep>>()

    init {
        loadData()
    }

    fun loadData() {
        isLoading.postValue(true)
        error.postValue(null)
        start(repository.extractCollection(EducationStep::class.java).resolve({
            educationSteps.value = it.toList().sortedByDescending { value -> value.startDate }
            isLoading.value = false
        }, {
            isLoading.value = false
            error.value = it
        }))
    }
}
