package com.kuba.cv.ui.experience

import android.view.View
import com.bumptech.glide.Glide
import com.kuba.cv.domain.entity.JobExperience
import com.kuba.cv.ui.R
import com.kuba.cv.ui.core.list.BaseRecyclerViewDelegate
import com.kuba.cv.ui.core.list.BaseRecyclerViewHolder
import com.kuba.cv.ui.utils.hide
import com.kuba.cv.ui.utils.isVisible
import com.kuba.cv.ui.utils.show
import kotlinx.android.synthetic.main.list_experience.*
import java.text.SimpleDateFormat
import java.util.Locale

class ExperienceDelegate(private val onClickListener: (Int) -> Unit) :
    BaseRecyclerViewDelegate<JobExperience, ExperienceDelegate.ViewHolder>(JobExperience::class.java) {
    override val layoutId: Int = R.layout.list_experience

    var expandedPosition = -1

    override fun createViewHolder(view: View) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseRecyclerViewHolder<JobExperience>(view) {
        override fun bindView(item: JobExperience, position: Int, itemsSize: Int) {
            experienceCompany.text = item.company
            experienceRole.text = item.role
            experiencePlace.text = item.place
            experienceDescription.text = item.description
            val formatter = SimpleDateFormat("MMM-YY", Locale.getDefault())
            experienceDate.text = if (item.endDate == null) {
                "${formatter.format(item.startDate)} - \n${containerView.context.getString(R.string.present)}"
            } else {
                "${formatter.format(item.startDate)} -\n${formatter.format(item.endDate)}"
            }
            experienceClickableRoot.setOnClickListener { onClickListener.invoke(position) }
            if (experienceDescription.isVisible() && expandedPosition != position) {
                experienceDescription.hide()
            } else if (!experienceDescription.isVisible() && expandedPosition == position) {
                experienceDescription.show()
            }
            Glide
                .with(containerView)
                .load(item.image)
                .centerCrop()
                .into(experienceCompanyImage)
        }
    }
}
