package com.kuba.cv.ui.experience

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.kuba.cv.ui.R
import com.kuba.cv.ui.core.BaseFragment
import com.kuba.cv.ui.core.list.BaseRecyclerAdapter
import com.kuba.cv.ui.core.list.CustomDivider
import com.kuba.cv.ui.databinding.FragmentExperienceBinding
import kotlinx.android.synthetic.main.fragment_experience.*
import org.koin.androidx.viewmodel.ext.android.getViewModel

class ExperienceFragment : BaseFragment<FragmentExperienceBinding>() {
    override val layoutId: Int = R.layout.fragment_experience

    private val adapter = BaseRecyclerAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel<ExperienceViewModel>().run {
            experiences.observe(this@ExperienceFragment, Observer { adapter.reloadItems(it) })
            error.observe(this@ExperienceFragment, Observer {
                it?.let { experienceError.text = getString(R.string.error_experience) }
            })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.model = getViewModel()
        adapter.addDelegate(ExperienceDelegate { onListItemClick(it) })
        experienceRecycler.addItemDecoration(CustomDivider())
        experienceRecycler.adapter = adapter
    }

    private fun onListItemClick(position: Int) {
        adapter.delegates.filterIsInstance<ExperienceDelegate>().firstOrNull()?.let {
            var oldPosition: Int? = null
            it.expandedPosition = when (it.expandedPosition) {
                -1 -> position
                position -> -1
                else -> {
                    oldPosition = it.expandedPosition
                    position
                }
            }
            oldPosition?.run { adapter.notifyItemChanged(this) }
            adapter.notifyItemChanged(position)
        }
    }
}
