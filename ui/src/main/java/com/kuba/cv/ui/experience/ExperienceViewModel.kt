package com.kuba.cv.ui.experience

import androidx.lifecycle.MutableLiveData
import com.kuba.cv.domain.Repository
import com.kuba.cv.domain.entity.JobExperience
import com.kuba.cv.ui.core.BaseViewModel
import com.kuba.cv.ui.utils.resolve

class ExperienceViewModel(private val repository: Repository) : BaseViewModel() {
    val isLoading = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable?>()
    val experiences = MutableLiveData<List<JobExperience>>()

    init {
        loadData()
    }

    fun loadData() {
        isLoading.postValue(true)
        error.postValue(null)
        start(repository.extractCollection(JobExperience::class.java).resolve({
            experiences.value = it.toList().sortedByDescending { value -> value.startDate }
            isLoading.value = false
        }, {
            isLoading.value = false
            error.value = it
        }))
    }
}
