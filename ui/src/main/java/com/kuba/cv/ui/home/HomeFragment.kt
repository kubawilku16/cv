package com.kuba.cv.ui.home

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.kuba.cv.ui.R
import com.kuba.cv.ui.core.BaseFragment
import com.kuba.cv.ui.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment<FragmentHomeBinding>() {
    override val layoutId: Int = R.layout.fragment_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let { prepareNavigation(Navigation.findNavController(it, R.id.homeNavHost)) }
    }

    private fun prepareNavigation(navController: NavController) {
        homeBottomNav.setupWithNavController(navController)
        homeBottomNav.setOnNavigationItemSelectedListener {
            return@setOnNavigationItemSelectedListener onItemSelected(it, navController)
        }
    }

    private fun onItemSelected(
        item: MenuItem,
        navController: NavController
    ): Boolean {
        return if (item.itemId == homeBottomNav.selectedItemId) false
        else {
            val builder = NavOptions.Builder()
                .setLaunchSingleTop(true)
                .setEnterAnim(androidx.navigation.ui.R.anim.nav_default_enter_anim)
                .setExitAnim(androidx.navigation.ui.R.anim.nav_default_exit_anim)
                .setPopEnterAnim(androidx.navigation.ui.R.anim.nav_default_pop_enter_anim)
                .setPopExitAnim(androidx.navigation.ui.R.anim.nav_default_pop_exit_anim)
                .setPopUpTo(homeBottomNav.selectedItemId, true)
            navController.navigate(item.itemId, null, builder.build())
            true
        }
    }
}
