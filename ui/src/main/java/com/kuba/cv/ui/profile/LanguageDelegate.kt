package com.kuba.cv.ui.profile

import android.view.View
import com.kuba.cv.domain.entity.Language
import com.kuba.cv.ui.R
import com.kuba.cv.ui.core.list.BaseRecyclerViewDelegate
import com.kuba.cv.ui.core.list.BaseRecyclerViewHolder
import kotlinx.android.synthetic.main.list_language.*

class LanguageDelegate :
    BaseRecyclerViewDelegate<Language, LanguageDelegate.ViewHolder>(Language::class.java) {

    override val layoutId: Int = R.layout.list_language

    override fun createViewHolder(view: View) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseRecyclerViewHolder<Language>(view) {
        override fun bindView(item: Language, position: Int, itemsSize: Int) {
            languageTitle.text = item.name
            languageLevel.text = item.level
        }
    }
}
