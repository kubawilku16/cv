package com.kuba.cv.ui.profile

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.kuba.cv.ui.R
import com.kuba.cv.ui.core.BaseFragment
import com.kuba.cv.ui.core.list.BaseRecyclerAdapter
import com.kuba.cv.ui.core.list.CustomDivider
import com.kuba.cv.ui.databinding.FragmentProfileBinding
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.androidx.viewmodel.ext.android.getViewModel


class ProfileFragment : BaseFragment<FragmentProfileBinding>() {
    override val layoutId: Int = R.layout.fragment_profile

    private val adapter = BaseRecyclerAdapter()

    private fun initObservers() {
        getViewModel<ProfileViewModel>().run {
            error.observe(this@ProfileFragment, Observer {
                it.let { profileError.text = getString(R.string.error_profile) }
            })
            languages.observe(this@ProfileFragment, Observer { adapter.reloadItems(it) })
            photo.observe(this@ProfileFragment, Observer {
                Glide
                    .with(this@ProfileFragment)
                    .load(it)
                    .centerCrop()
                    .placeholder(R.drawable.ic_cv)
                    .into(profileImage)
            })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.model = getViewModel()
        adapter.addDelegate(LanguageDelegate())
        profileLanguagesRecycler.adapter = adapter
        profileLanguagesRecycler.addItemDecoration(CustomDivider())
        initListeners()
    }

    private fun initListeners() {
        profilePhone.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:${profilePhone.text}")
            startActivity(intent)
        }
        profileEmail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/html"
            intent.putExtra(Intent.EXTRA_EMAIL, profileEmail.text)
            startActivity(Intent.createChooser(intent, getString(R.string.send_email)))
        }
        profileLinkedIn.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(profileLinkedIn.text.toString())
                intent.setPackage("com.linkedin.android")
                startActivity(intent)
            } catch (ignored: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(profileLinkedIn.text.toString())
                    )
                )
            }
        }
        profileGit.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(profileGit.text.toString())))
        }
        profileFab.setOnClickListener {
            ClauseDialog().show(childFragmentManager, ClauseDialog::class.java.simpleName)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObservers()
    }
}
