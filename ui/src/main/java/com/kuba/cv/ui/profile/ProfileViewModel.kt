package com.kuba.cv.ui.profile

import androidx.lifecycle.MutableLiveData
import com.kuba.cv.domain.Repository
import com.kuba.cv.domain.entity.Language
import com.kuba.cv.domain.entity.Profile
import com.kuba.cv.ui.core.BaseViewModel
import com.kuba.cv.ui.utils.resolve

class ProfileViewModel(private val repository: Repository) : BaseViewModel() {
    val isLoading = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable?>()
    val firstName = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val phone = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val git = MutableLiveData<String>()
    val linkedIn = MutableLiveData<String>()
    val languages = MutableLiveData<List<Language>>()
    val photo = MutableLiveData<String>()
    val about = MutableLiveData<String>()

    init {
        loadData()
    }

    fun loadData() {
        isLoading.postValue(true)
        error.postValue(null)
        start(repository.extractData(Profile::class.java).resolve({
            firstName.value = it.firstName
            lastName.value = it.lastName
            phone.value = it.phone
            email.value = it.email
            git.value = it.git
            linkedIn.value = it.linkedIn
            photo.value = it.photo
            about.value = it.about
            languages.value = it.languages.map { (key, value) -> Language(key, value) }
            isLoading.value = false
        }, {
            isLoading.value = false
            error.value = it
        }))
    }
}
