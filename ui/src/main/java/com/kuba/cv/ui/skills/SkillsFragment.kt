package com.kuba.cv.ui.skills

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.kuba.cv.ui.R
import com.kuba.cv.ui.core.BaseFragment
import com.kuba.cv.ui.core.list.BaseRecyclerAdapter
import com.kuba.cv.ui.core.list.CustomDivider
import com.kuba.cv.ui.databinding.FragmentSkillsBinding
import kotlinx.android.synthetic.main.fragment_skills.*
import org.koin.androidx.viewmodel.ext.android.getViewModel

class SkillsFragment : BaseFragment<FragmentSkillsBinding>() {
    override val layoutId: Int = R.layout.fragment_skills

    private val adapter = BaseRecyclerAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel<SkillsViewModel>().run {
            error.observe(this@SkillsFragment, Observer {
                it?.let { skillsError.text = getString(R.string.error_education) }
            })
            skills.observe(this@SkillsFragment, Observer { adapter.reloadItems(it) })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.model = getViewModel()
        adapter.addDelegate(SkillsSetDelegate { onListItemClick(it) })
        skillsRecycler.addItemDecoration(CustomDivider())
        skillsRecycler.adapter = adapter
    }

    private fun onListItemClick(position: Int) {
        adapter.delegates.filterIsInstance<SkillsSetDelegate>().firstOrNull()?.let {
            var oldPosition: Int? = null
            it.expandedPosition = when (it.expandedPosition) {
                -1 -> position
                position -> -1
                else -> {
                    oldPosition = it.expandedPosition
                    position
                }
            }
            oldPosition?.run { adapter.notifyItemChanged(this) }
            adapter.notifyItemChanged(position)
        }
    }
}
