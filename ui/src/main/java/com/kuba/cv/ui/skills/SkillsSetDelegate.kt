package com.kuba.cv.ui.skills

import android.view.View
import com.kuba.cv.domain.entity.SkillSet
import com.kuba.cv.ui.R
import com.kuba.cv.ui.core.list.BaseRecyclerViewDelegate
import com.kuba.cv.ui.core.list.BaseRecyclerViewHolder
import com.kuba.cv.ui.utils.hide
import com.kuba.cv.ui.utils.isVisible
import com.kuba.cv.ui.utils.show
import kotlinx.android.synthetic.main.list_skills.*

class SkillsSetDelegate(private val onClickListener: (Int) -> Unit) :
    BaseRecyclerViewDelegate<SkillSet, SkillsSetDelegate.ViewHolder>(SkillSet::class.java) {

    var expandedPosition = -1

    override val layoutId: Int = R.layout.list_skills

    override fun createViewHolder(view: View) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseRecyclerViewHolder<SkillSet>(view) {
        override fun bindView(item: SkillSet, position: Int, itemsSize: Int) {
            skillsTitle.text = item.title
            skillsTitle.setOnClickListener { onClickListener.invoke(position) }
            skillsContent.text = item.values.joinToString("\n")
            if (skillsContent.isVisible() && expandedPosition != position) {
                skillsContent.hide()
            } else if (!skillsContent.isVisible() && expandedPosition == position) {
                skillsContent.show()
            }
        }
    }
}
