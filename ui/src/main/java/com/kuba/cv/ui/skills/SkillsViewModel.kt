package com.kuba.cv.ui.skills

import androidx.lifecycle.MutableLiveData
import com.kuba.cv.domain.Repository
import com.kuba.cv.domain.entity.SkillSet
import com.kuba.cv.ui.core.BaseViewModel
import com.kuba.cv.ui.utils.resolve

class SkillsViewModel(private val repository: Repository): BaseViewModel() {
    val isLoading = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable?>()
    val skills = MutableLiveData<List<SkillSet>>()

    init {
        loadData()
    }

    fun loadData() {
        isLoading.postValue(true)
        error.postValue(null)
        start(repository.extractCollection(SkillSet::class.java).resolve({
            skills.value = it.toList()
            isLoading.value = false
        }, {
            isLoading.value = false
            error.value = it
        }))
    }
}
