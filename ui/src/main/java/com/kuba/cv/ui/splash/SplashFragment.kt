package com.kuba.cv.ui.splash

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.kuba.cv.ui.R
import org.koin.androidx.viewmodel.ext.android.getViewModel

class SplashFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel<SplashViewModel>().finished.observe(this, Observer { goToHome() })
    }

    private fun goToHome() {
        activity?.let {
            Navigation.findNavController(it, R.id.mainNavHost)
                .navigate(R.id.action_splashFragment_to_homeFragment)
        }
    }
}
