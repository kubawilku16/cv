package com.kuba.cv.ui.splash

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import com.kuba.cv.ui.core.BaseViewModel
import com.kuba.cv.ui.utils.notify

class SplashViewModel : BaseViewModel() {
    val finished = MutableLiveData<Unit>()

    init {
        Handler().postDelayed({ finished.notify() }, DELAY)
    }

    companion object {
        private const val DELAY = 500L
    }
}
