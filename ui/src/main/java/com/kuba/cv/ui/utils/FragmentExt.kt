package com.kuba.cv.ui.utils

import androidx.fragment.app.Fragment

fun Fragment.hideKeyboard() {
    this.view?.hideKeyboard()
}

fun Fragment.showKeyboard() {
    this.view?.showKeyboard()
}
