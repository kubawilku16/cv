package com.kuba.cv.ui.utils

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun <T> Observable<T>.resolve(onNext: (T) -> Unit): Disposable {
    return resolve(onNext, {}, {})
}

fun <T> Observable<T>.resolve(onNext: (T) -> Unit, onError: (Throwable) -> Unit): Disposable {
    return resolve(onNext, onError, {})
}

fun <T> Observable<T>.resolve(
    onNext: (T) -> Unit,
    onError: (Throwable) -> Unit,
    onComplete: () -> Unit
): Disposable {
    return subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { onNext.invoke(it) },
            {
                it.printStackTrace()
                onError.invoke(it)
            },
            { onComplete.invoke() })
}

fun <T> Single<T>.resolve(onNext: (T) -> Unit): Disposable {
    return resolve(onNext, {})
}

fun <T> Single<T>.resolve(onNext: (T) -> Unit, onError: (Throwable) -> Unit): Disposable {
    return subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { onNext.invoke(it) },
            {
                it.printStackTrace()
                onError.invoke(it)
            })
}

fun Completable.resolve(onNext: () -> Unit): Disposable {
    return resolve(onNext, {})
}

fun Completable.resolve(onNext: () -> Unit, onError: (Throwable) -> Unit): Disposable {
    return subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { onNext.invoke() },
            {
                it.printStackTrace()
                onError.invoke(it)
            })
}

fun <T> Maybe<T>.resolve(onNext: (T) -> Unit): Disposable {
    return resolve(onNext, {})
}

fun <T> Maybe<T>.resolve(onNext: (T) -> Unit, onError: (Throwable) -> Unit): Disposable {
    return subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { onNext.invoke(it) },
            {
                it.printStackTrace()
                onError.invoke(it)
            })
}
