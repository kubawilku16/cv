package com.kuba.cv.ui.utils

import android.content.Context
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationSet
import android.view.animation.ScaleAnimation
import android.view.inputmethod.InputMethodManager

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.isVisible() = this.visibility == View.VISIBLE

fun View.show(duration: Long = 500L) {
    val alphaAnim = AlphaAnimation(0f, 1f)
    alphaAnim.startTime = 0
    alphaAnim.duration = duration

    val scaleAnim = ScaleAnimation(0f, 1f, 0f, 1f)
    scaleAnim.duration = duration
    scaleAnim.startTime = 0

    val animation = AnimationSet(true)
    animation.addAnimation(alphaAnim)
    animation.addAnimation(scaleAnim)

    this.animation = animation
    this.animate()
    this.visible()
}

fun View.hide(duration: Long = 500L) {
    val alphaAnim = AlphaAnimation(1f, 0f)
    alphaAnim.startTime = 0
    alphaAnim.duration = duration

    val scaleAnim = ScaleAnimation(1f, 0f, 1f, 0f)
    scaleAnim.duration = duration
    scaleAnim.startTime = 0

    val animation = AnimationSet(true)
    animation.addAnimation(alphaAnim)
    animation.addAnimation(scaleAnim)

    this.animation = animation
    this.animate()
    this.gone()
}

fun View.showKeyboard() {
    (this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun View.hideKeyboard() {
    (this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(this.windowToken, 0)
    this.isFocusable = false
    this.isFocusableInTouchMode = false
    this.isFocusable = true
    this.isFocusableInTouchMode = true
    this.requestFocus()
}
